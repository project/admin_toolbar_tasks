<?php

namespace Drupal\admin_toolbar_tasks;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Menu\LocalTaskManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class AdminToolbarTasksBuilder implements ContainerInjectionInterface, TrustedCallbackInterface {

  private LocalTaskManagerInterface $localTaskManager;

  private RouteMatchInterface $routeMatch;

  public function __construct(LocalTaskManagerInterface $local_task_manager, RouteMatchInterface $route_match) {
    $this->localTaskManager = $local_task_manager;
    $this->routeMatch = $route_match;
  }

  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('plugin.manager.menu.local_task'),
      $container->get('current_route_match')
    );
  }

  public static function trustedCallbacks(): array {
    return ['build'];
  }

  public function build(): array {
    $cacheability = new CacheableMetadata();

    $build = [
      '#theme' => 'links__admin_toolbar_tasks',
      '#links' => $this->getAdminTasks($cacheability),
    ];

    $cacheability->applyTo($build);

    return $build;
  }

  private function getAdminTasks(CacheableMetadata $cacheability): array {
    $cacheability->addCacheContexts(['route']);

    $tabs = $this->localTaskManager->getLocalTasks($this->routeMatch->getRouteName())['tabs'];

    usort(
      $tabs,
      fn($a, $b) => $a['#weight'] ?? 0 <=> $b['#weight'] ?? 0
    );

    $links = [];
    foreach ($tabs ?? [] as $tab_id => $tab) {
      if (empty($tab['#admin_toolbar_tasks'])) {
        continue;
      }

      $access = $tab['#admin_toolbar_tasks_access'] ?? NULL;
      if (!$access instanceof AccessResultInterface|| !$access->isAllowed()) {
        continue;
      }

      $cacheability->addCacheableDependency($tab['#admin_toolbar_tasks_access']);

      $links[$tab_id] = [
        'type' => 'link',
        'title' => $tab['#link']['title'],
        'url' => $tab['#link']['url'],
      ];
    }

    return $links;
  }

}
