CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Admin Toolbar Tasks module splits of and displays administrative local
tasks as part of the site Toolbar.

 * For a full description of the module visit:
   https://www.drupal.org/project/admin_toolbar_tasks

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/admin_toolbar_tasks


REQUIREMENTS
------------

This module requires the following module:

 * Toolbar - https://www.drupal.org/project/drupal

This module works well together with:

 * Admin Toolbar - https://www.drupal.org/project/admin_toolbar


INSTALLATION
------------

Install the Admin Toolbar Tasks module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.


MAINTAINERS
-----------

 * casey - https://www.drupal.org/u/casey

Supporting organization:

 * SWIS - https://www.drupal.org/swis
